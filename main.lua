local physics = require( "physics")

local centerX = display.contentCenterX
local centerY = display.contentCenterY

--表示を隠す
display.setStatusBar( display.HiddenStatusBar )

--物理エンジンスタート
physics.start()

--背景の空の表示
local sky = display.newImage( "sky.jpg", centerX, centerY )

--ジャンプボタンの作成
local jumps = display.newImage("jump.png")
	jumps.x = display.contentWidth*0.75
	jumps.y = display.contentHeight*0.3
    jumps.xScale = 0.1
    jumps.yScale = 0.1
 

local function onJump(event)

end

local jumpmodel ={-30,30,30,30,30,-30,-30,-30} --物理エンジンに登録する形

--ボタンを物理エンジンに登録
for index,jump in pairs(jumps) do
	physics.addBody( jumps, "kinematic", { denisity=0, friction=0, bounce=0, shape=jumpmodel, isSensor=true})
    -- physics.addBody( buttons, { radius=30,isSensor=true })
	
	jumps:setLinearVelocity(0,0)
	--ボタンのタップイベントの登録
	jumps:addEventListener("tap",onJump)
end


--ファイヤーボタンの作成
local buttons = display.newImage("button.png")
	buttons.x = display.contentWidth*0.9
	buttons.y = display.contentHeight*0.3
    buttons.xScale = 0.2
    buttons.yScale = 0.2
 

local function onButton(event)
    
end

local buttonmodel ={-30,40,30,40,30,-40,-30,-40} --物理エンジンに登録する形

--ファイヤーボタンを物理エンジンに登録
for index,button in pairs(buttons) do
	physics.addBody( buttons, "kinematic", { denisity=0, friction=0, bounce=0, shape=buttonmodel, isSensor=true})
   
		buttons:setLinearVelocity(0,0)
	--ファイヤーボタンのタップイベントの登録
	buttons:addEventListener("tap",onButton)
end


local scorePoint = 0
--クリア画面の作成
local function clear()
	local backred = display.newRect( centerX,centerY,display.contentWidth,display.contentHeight )
    backred:setFillColor( 255,0,0)
    display.newText("CLEAR",centerX,centerY,nil,40 )
    display.newText("SCORE"..scorePoint,centerX,centerY+100,nil,20 )
end

--失敗画面の作成
local function failed()
	local backblue = display.newRect(  centerX,centerY, display.contentWidth,display.contentHeight)
	backblue:setFillColor(0,0,255)
	display.newText("FAILURE",centerX,centerY,nil,40)
	display.newText("SCORE"..scorePoint,centerX,centerY+100,nil,20)
end

--木１の表示
local trees1 = {}
for i =1,10 do
	trees1[i] = display.newImage("grass3.png")
	trees1[i].x=(centerX + 300)*i
	trees1[i].y=display.contentHeight - 40
	trees1[i].xScale=0.25
	trees1[i].yScale=0.5
end

--木１を物理エンジンに登録
for index,tree1 in pairs( trees1 )do
	physics.addBody( tree1, "kinematic", { denisity=0,friction=0, bounce=0,shape=hartmodel,isSensor=true})
	tree1:setLinearVelocity(-100,0)
end

--木２の表示
local trees2={}
for i=1,10 do
    trees2[i] = display.newImage("tree4.png")
    trees2[i].x =(centerX + 400)*i
    trees2[i].y =display.contentHeight-40
    trees2[i].xScale=0.3
    trees2[i].yScale=0.7
end

--木２を物理エンジンに登録
for index,tree2 in pairs( trees2 )do
	physics.addBody( tree2,"kinematic", { denisity=0,friction=0,bounce=0,shape=hartmodel,isSensor=true})
	tree2:setLinearVelocity(-60,0)
end

--木３の表示
local trees3 = {}
for i=1,10 do
	trees3[i] = display.newImage("tree5.png")
	trees3[i].x = (centerX + 500) * i
	trees3[i].y = display.contentHeight-100
	trees3[i].xScale = 0.25
	trees3[i].yScale = 0.6
end

--木３を物理エンジンに登録
for index,tree3 in pairs(trees3) do
	physics.addBody( tree3, "kinematic", { denisity=0, friction=0, bounce=0, shape=hartmodel, isSensor=true } )
	tree3:setLinearVelocity(-30,0)
end

local collisionOnGround = true

--人シートの作成
local humanImageSheet = graphics.newImageSheet( "runningman2.png", { width=88,height=80,numFrames=7 } )
--人のアニメーションを表示7フレームを0.6秒で表示
local humanAnimation = display.newSprite(humanImageSheet,{name="human",start=1,count=7,time=600 } )
humanAnimation.x = display.contentWidth * 0.2 --位置を指定
humanAnimation.y = 300
humanAnimation.xScale = 0.5   --サイズを指定
humanAnimation.yScale = 1.0
local humanmodel = {-22,40,22,40,22,-40,-22,-40} --物理エンジンに登録する用の形
physics.addBody( humanAnimation, { denisity=1,friction=1,bounce=0,shape=humanmodel } )
--人を物理エンジンに登録
humanAnimation:play() --再生

local function onGround(event)
	if event.phase=="began" and collisionOnGround == false then
		collisionOnGround = true
		humanAnimation:play()
	end
end

--芝の下の層
local greenGround = display.newRect(0,0,2000,40)
greenGround:setFillColor( 0x31/255,0x5a/255,0x18/255)
greenGround.x = centerX
greenGround.y = 315
physics.addBody( greenGround,"static",{} )--芝の下の層を物理エンジンに登録
greenGround:addEventListener("collision",onGround)

--ジャンプタップイベント
local function humanJump(event)
	if collisionOnGround == true then --人の位置が地面にあるとき
		collisionOnGround=false
		humanAnimation:pause()
		humanAnimation:applyLinearImpulse(0,-0.4,humanAnimation.x,humanAnimation.y)
		humanAnimation.isFixedRotation = true --人が回転しないようにしている
	end
end

--ジャンプタップイベントの登録
jumps:addEventListener("tap",humanJump)

--火玉の作成
local function fireLaser (event)

   local fires = display.newImage("button.png")
    fires.xScale = 0.1
    fires.yScale = 0.1
    -- physics.setGravity( 0, 0)

    physics.addBody( fires,"dynamic",{ isSensor = true })
    fires.isBullet = "true"
    fires.myName = "fires"

    fires.x = humanAnimation.x      
    fires.y = humanAnimation.y-10


transition.to( fires,{ x = 600,time = 1000,
	onComplete = function()
	display.remove(fires)
	end } )
end
buttons:addEventListener( "tap", fireLaser )


--hpの表示

local hpPoint = 300
local hpdisplay = display.newText("HP"..hpPoint,0,0,nil,20)
hpdisplay.x=display.contentWidth * 0.1
hpdisplay.y=display.contentHeight * 0.1

--スコアの表示
local scoredisplay = display.newText( "SCORE:"..scorePoint, 0, 0, nil, 20 )
scoredisplay.x = display.contentWidth * 0.8
scoredisplay.y = display.contentHeight * 0.1

--ヘビの表示
local snakes = {}
for i=1,10 do
snakes[i] = display.newImage( "snake3.png" )
snakes[i].x = (centerX + 550) * i
snakes[i].y = display.contentHeight * 0.85
snakes[i].xScale = 0.3
snakes[i].yScale = 0.3
end

local function onSnake( event )
    if ( event.phase == "began") then
        local snake = event.target
        display.remove(snake)
        hpPoint = hpPoint - 100
        hpdisplay.text = "HP:"..hpPoint

 -- HP がゼロになったら失敗画面を表示する
        if hpPoint == 0 then
            failed()
        end

    end
end

local snakemodel = { -15, 23, 10, 23, 10, -15, -15, -15 }--物理エンジンに登録する用の形
--ヘビを物理エンジンに登録
for index,snake in pairs( snakes ) do
    physics.addBody( snake, "kinematic", { denisity=0, friction=1, bounce=0, shape=mousemodel, isSensor=true } )

    -- ヘビを左に動かす
    snake:setLinearVelocity(-200,0)

    --ヘビへの接触イベントの登録
    snake:addEventListener( "collision", onSnake )
end

-- --ヘビの表示
-- local snakes = {}
-- for i = 1,10 do
-- 	snakes[i]= display.newImage("snake.jpg")
-- 	snakes[i].x = (centerX + 550) * i
-- 	snakes[i].y = display.contentHeight * 0.85
-- 	snakes[i].xScale = 0.2
-- 	snakes[i].yScale = 0.3
-- end

-- local function onSnake( event )
-- 	if ( event.phase == " began ") then
-- 		 local snake = event.target
-- 		 display.remove( snake ) 

-- 	    hpPoint = hpPoint - 100
-- 	    hpdisplay.text = "HP:"..hpPoint

-- 	    if hpPoint == 0 then
-- 		    failed()
-- 	    end
--     end
-- end

-- local snakemodel = { -20,25,20,25,20,-25,-20,-25 }--物理エンジンに登録する用の形

-- --ヘビを物理エンジンに登録
-- for index,snake in pairs( snakes ) do
-- 	physics.addBody( snake, "kinematic", { denisity=0, friction=1, bounce=0, shape=snakemodel, isSensor=true } )
--     snake:setLinearVelocity(-200,0)
--     snake:addEventListener( "collision", onSnake )
--     snake:addEventListener( "collision", onSnake )
-- end

--芝の表示
local grassposi = 0
local grasses = {}
for i=1,30 do
	grasses[i] = display.newImage("grass6.png")
	grasses[i].x = grassposi
	grasses[i].y = display.contentHeight-20
	grasses[i].xScale = 1.0
	grasses[i].yScale = 0.6
	grassposi = centerX * i
end

--芝を物理エンジンに登録
for index,grass in pairs(grasses ) do
	physics.addBody( grass,"kinematic",{ denisity=0,friction=0,bounce=0,isSensor=true } )
	grass:setLinearVelocity( -100,0 )
end

--コインを表示
local coins = {}
for i=1,10 do
    coins[i] = display.newImage( "coin2.png" )
    coins[i].x = (centerX + 350) * i
    coins[i].y = display.contentHeight * 0.5
    coins[i].xScale = 0.2
    coins[i].yScale = 0.2
end

local function onCoin( event )
	if( event.phase == "began" ) then
		display.remove(event.target)
		event.target = nil
		scorePoint = scorePoint + 50
		scoredisplay.text = "SCORE"..scorePoint --得点の更新
	end
end

local coinmodel = {-22,22,22,22,22,-22,-22,-22}--物理エンジンに登録する用の形
--コインを物理エンジンに登録
for index,coin in pairs(coins) do
	physics.addBody( coin, "kinematic" ,{ denisity=0,friction=0,bounce=0,shape=coinmodel,isSensor=true } )
	coin:setLinearVelocity(-70,0)
	--コインへの接触イベントの登録
	coin:addEventListener("collision",onCoin)
end

--ハートの表示
local harts = {}
for i = 1,10 do
	harts[i] = display.newImage( "hart3.png" )
	harts[i].x = (centerX + 600) * i
	harts[i].y = display.contentHeight * 0.5
	harts[i].xScale = 0.3
	harts[i].yScale = 0.3
end

local function onHart(event)
	if(event.phase=="began") then
        display.remove(event.target)
        event.target = nil
        hpPoint = hpPoint + 100
        hpdisplay.text="HP:"..hpPoint --hpの上昇の更新
    end
end

local hartmodel ={-20,16,20,16,20,-16,-20,-16} --物理エンジンに登録する形

--ハートを物理エンジンに登録
for index,hart in pairs(harts) do
	physics.addBody( hart, "kinematic", { denisity=0, friction=0, bounce=0, shape=hartmodel, isSensor=true})
	hart:setLinearVelocity(-70,0)
	--ハートへの接触イベントの登録
	hart:addEventListener("collision",onHart)
end

--クリアの判定
local function resultDisplay( event )
	if scorePoint > 0 and hpPoint > 200 then --コインが50以上ならクリア？
		clear()

    elseif scorePoint <= 0 then--コインが０なら失敗
    	failed()
    end
end

local DELAYTIME = 30*1000
timer.performWithDelay( DELAYTIME, resultDisplay) --DELAYTIME後に関数実行


